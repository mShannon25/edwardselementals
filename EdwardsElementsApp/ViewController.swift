//
//  ViewController.swift
//  EdwardsElementsApp
//
//  Created by MattShannon on 10/21/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, UIGestureRecognizerDelegate {

    @IBOutlet var containerView: UIView!
    
    @IBOutlet var homeBtn: UIBarButtonItem!
    @IBOutlet var backBtn: UIBarButtonItem!
    @IBOutlet var forwardBtn: UIBarButtonItem!
    var webView: WKWebView!
   // var loadingIndicator: UIActivityIndicatorView!
   
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!

   // let url = NSURL(string:"http://interactpolicy.staging.wpengine.com/")
    let url = NSURL(string:"http://interactpolicy.staging.wpengine.com/")

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      //  toolbarHeight = toolbar.frame.size.height
      //  print("Toolbar height ",toolbarHeight)

        loadWebView();
        
      //  self.addNewIndicator()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    // TooLBar
    @IBAction func doRefresh(_ sender: AnyObject) {
        webView.reload()
    }
    @IBAction func goHome(_ sender: AnyObject) {
        if (webView.canGoBack) {
            loadWebView()
        }
    }
    
    @IBAction func goForward(_ sender: AnyObject) {
        if webView.canGoForward{
            webView.goForward()
        }
    }

    @IBAction func goBack(_ sender: AnyObject) {
        if webView.canGoBack{
            webView.goBack()
        }
    }
    
    // Webview
    func loadWebView(){
      //  print("Toolbar height ",toolbar.frame.size.height)
        webView = WKWebView(frame: CGRect( x: 0, y: 0, width: containerView.bounds.width, height: containerView.bounds.height - 0 ), configuration: WKWebViewConfiguration() )
        let req = NSURLRequest(url:url! as URL)
        webView.load(req as URLRequest)
        webView.allowsBackForwardNavigationGestures = true
       // webView.allowsLinkPreview = false;
        webView.scrollView.isScrollEnabled = true
        webView.isUserInteractionEnabled = true
        webView.navigationDelegate = self
       // self.view = self.webView
        self.containerView.addSubview(webView)
        webView?.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin]
        
        let longPressRecogniser = UILongPressGestureRecognizer(target: self, action: #selector(onLongPress(_:)))
        longPressRecogniser.delegate = self
        longPressRecogniser.minimumPressDuration = 1000
        
       // webView.evaluateJavaScript("document.body.style.webkitTouchCallout='none';", completionHandler: nil)
       // webView.scrollView.addGestureRecognizer(longPressRecogniser)
        
       // self.edgesForExtendedLayout = UIRectEdgeNone
      //
        print("WebView Loaded ")
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func onLongPress(_ gestureRecognizer:UILongPressGestureRecognizer!){
       // print("WTF")
        if gestureRecognizer.state == UIGestureRecognizerState.began {
            //longPressSwitch = true
            print("LONG PRESS")
          //  gestureRecognizer.isEnabled = false
        }
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
       // print("Decision ",navigationAction.navigationType)
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        decisionHandler(.allow)
    }
    // This cancels all alert sheets
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        
        guard let alertController = viewControllerToPresent as? UIAlertController,
            alertController.preferredStyle == .actionSheet else {
                // Not an alert controller, present normally
                super.present(viewControllerToPresent, animated: flag, completion: completion)
                return
        }
    }
    
    // Indicator
 /*   func addNewIndicator() {
        if loadingIndicator == nil {
            loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            loadingIndicator.center = view.center

            loadingIndicator.startAnimating()
            containerView.addSubview(loadingIndicator)
            
        }
       // return loadingIndicator
    }*/
   /* func placeAtTheCenterWithView(view: UIView) {
        
        self.view.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: 0))
        
        self.view.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: 0))
    }
 */
    // Delegate Methods
    
    // Navigation Started
    func webView(_ WKWebView: WKWebView, didCommit: WKNavigation!){
        if (loadingIndicator != nil) {
            loadingIndicator.startAnimating()
            loadingIndicator.hidesWhenStopped = true;
        }
        if (webView != nil){
            backBtn.isEnabled = webView.canGoBack
            forwardBtn.isEnabled = webView.canGoForward
            homeBtn.isEnabled = webView.canGoBack
            
        }
    }
    // Navigation Completed
    func webView(_ wv: WKWebView, didFinish navigation: WKNavigation!){
        if loadingIndicator != nil {
            loadingIndicator.stopAnimating()
        }
    }
    // NETWORK ERROR
    func webView(_ wv: WKWebView, didFailProvisionalNavigation: WKNavigation!, withError: Error){
        print("ERROR ",withError.localizedDescription)
        let error = withError.localizedDescription
        let alert = UIAlertController(title:"Error",message:error,preferredStyle:UIAlertControllerStyle.alert);
        let ok = UIAlertAction(title:"Try Again",style: UIAlertActionStyle.default){(ACTION) in
            //self.loadWebView()
            self.webView.reload()
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
        if loadingIndicator != nil {
            loadingIndicator.stopAnimating()
        }
    }
    
}

//extension ViewController: WKNavigationDelegate {
//    
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        print("Finished navigating to url \(webView.url)");
//    }
//    
//}
